import RenderDOM from 'react-dom';

class Render {
    render() {
        let tpl = "";
        localStorage.setItem("template", "route");
        localStorage.getItem('template') ? tpl = localStorage.getItem("template") : tpl = "auth";
        document.querySelector("div").setAttribute("id", tpl);
        let App = require('../views/'+tpl+'').default;
        RenderDOM.render(<App />, document.getElementById(tpl));
    }
}

const render = new Render();
export default render;
