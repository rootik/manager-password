import {Router, Route, Link, browserHistory} from 'react-router'
import { Provider } from 'react-redux'
import App from './ui/auth/auth.jsx'
import Main from './ui/main/main'
import store from '../stores/store'

class Routes extends View {
  constructor(){
    super();

    console.log("test");
  }

  render(){
    return(
        <Provider store={store}>
          <Router history={browserHistory}>
            <Route path="/" component={App}>
            </Route>
            <Route path="/main" component={Main}></Route>
          </Router>
        </Provider>
    );
  }
}

export default Routes;
