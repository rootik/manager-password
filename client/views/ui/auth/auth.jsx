import { connect } from 'react-redux'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import auth from './auth'
import auth_css from './auth.scss'

class Auth extends View {
    constructor() {
        super();
        console.log(this.props);
    }

    addTrack() {
      console.log(this.props);
    }

    render() {
        console.log(this.props.testStore);

        return (
            <div className="container">
                <div className="row">
                    hello
                    <input type='text' ref={(input)=>{this.trackInput = input}}/>
                    <button onClick={this.addTrack.bind(this)}>add test</button>
                </div>
            </div>
        );
    }
}

Auth.childContextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
};

export default connect(
  state => ({
    testStore:state
  }),
  dispatch => ({})
)(Auth);
