import {createStore, combineReducers, applyMiddleware} from 'redux'
import * as reducers from '../reducers'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

const reducer = combineReducers(reducers);

const store = createStore(reducer, {
  counter:0,
  track:[]
} ,composeWithDevTools(applyMiddleware(thunk)));

export default store;
