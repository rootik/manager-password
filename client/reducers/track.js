export default function track(state=[], action){
  switch (action.type) {
    case "ADD_TRACK":
      state.push(action.payload);
      return state;
    default:
      return state;
  }
}
