import React from 'react'
import View from '../classes/view'
import Model from '../classes/model';
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin();

export class baseController {
  constructor() {
    require("../lib/bootstrap/_bootstrap.scss");
    require("../views/styles/style.scss");
    require("../lib/font-awesome/scss/font-awesome.scss");
    global.React = React;
    global.View = View;
    global.Model = Model;
    global.render = require("../classes/render").default;

    // Putting all the files in the ui

    render.render();
  }
}

let base = new baseController();
export default base;
