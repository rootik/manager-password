class Users extends Model {
    constructor() {
        super();
        this.http = this.http;
    }

    signin() {
        var email = document.getElementById("Email");
        var password = document.getElementById("Password");

        this.http({
            type:"POST",
            url:"users/sign_in",
            data:{email:email.value, password:password.value}
        }).then(result =>{
            var data = JSON.parse(result);

            if(data.result==true) {
                localStorage.setItem("users", JSON.stringify(data.users));

                //Template render
                localStorage.setItem("template","app");
                render.render();
            } else if(data.result==false) {
                let block = document.getElementById("message");
                block.classList.add("show");
                block.classList.add("error");
                block.innerHTML = data.message;

                email.value = "";
                password.value = "";

                setTimeout(()=>{
                    block.classList.remove("show");
                    block.classList.remove("error");
                },3000);
            }
        });
    }

    signup(){
        var email = document.getElementById("Email");
        var password = document.getElementById("Password");

        this.http({
            type:"POST",
            url:"users/sign_up",
            data:{email:email.value, password:password.value}
        }).then(result =>{
            let data = JSON.parse(result);
            let block = document.getElementById("message");
            block.classList.add("show");
            block.innerHTML = data.message;
            
            if(data.result==true){
                block.classList.add("success");
                
            }else if(data.result==false) {
                block.classList.add("error");
            }
                    
            email.value = "";
            password.value = "";

            setTimeout(()=>{
                block.classList.remove("show");
                block.classList.remove("error");
                block.classList.remove("success");
            },3000);
        });
    }

    logout() {
        for (var prop in localStorage) {
            localStorage.removeItem(prop);
        }
        render.render();
    }
}

const users = new Users;
export default users;