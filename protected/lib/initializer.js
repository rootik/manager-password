module.exports = class Init {
    constructor() {
        this.setDefine();
        this.setModules();
        this.setStatic();
    }

    setDefine() {
        let define = require("../../config").define;

        for (var prop in define) {
            if(define.hasOwnProperty(prop)) global[prop] = define[prop];
        }
    }

    setModules() {
        let modules = require("../../config").modules;

        for (var prop in modules) {
            if(modules.hasOwnProperty(prop))  global[prop] = require(modules[prop]);
        }
    }

    setStatic() {
        let modules = fs.readdirSync(_dir_statics);

        for (var prop in modules) {
            if(modules.hasOwnProperty(prop)){
                let reg = new RegExp("[a-z A-Z]{0,}");
                let name = modules[prop].match(reg)[0];
                global[name] = require(_dir_statics + modules[prop]);
            } 
        }
    }
}